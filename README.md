# Quickstart TWS wordpress Website 2020 (Don't Push Files to Master Branch)

## Our main vision while working on this project, we must take care about Safety, Security, Backup, Optimization of this project.

---
## Few Major Points & Information before start work

UI :- [Click here](https://app.evrybo.com/share/project/28526/372609/bKwcfmMayir9yvzrIeDo#373233)

Needs to Update About Page as per UI Design :- \\tws-server\Comman Share\TWS-Site-Pages-2020

BitBucket URL :- [Click here](https://bitbucket.org/tekkiwebsolutions/tws-2020/src/master/)

Local Server URL :- [Click here](http://175.176.184.243:8088/)

Admin URL :- [Click here](http://175.176.184.243:8088/wp-admin/) Username :- tws-admin-2020 / Password :- Q6wYu6Ui65(AwPp!Z1

Testing Server URL :- [Click here](https://tekkiwebsolutions.net/)

Production Server URL :- [Click here](http://13.58.24.104/)

SSH User :- bitnami

---

# Important Points while working on this Website

1. Images should be optimzed into .webp format before upload on Server. Images path should DOMAINNAME/assets/images/2020/MONTH/IMAGENAME
2. Images NAME should be as per Object purpose.
3. Logos and Icons should be in SVG format.
4. All Plugins should be Updated
5. Each Page speed should be less than 2-3 Seconds.
6. CSS & JS & HTML should be load in minified version.
7. We needs to work into Child Theme, we will not work into Core Theme. and Child Theme name will be "tekkiwebsolutions".
8. MailChimp & Bitrix form should be implemented for Marketing and Lead Management
9. All forms leads should come to Bitrix CRM, including Schedule a Meeting Popup
10. Blog Posts, Comment section shouldbe secure via Captcha or Discuss Plugin.
11. If multiple developer work on same time, then developer must be push files into their name branch
12. Images regenerated version not needed.

---

AWS WP-Admin Login Details
=======================
admin-tws-2020 / flIqh#mFWtjm5mZdNYguNG&s

---

AWS PHPMyAdmin Login Details
========================
user / cjNWw9zmygMo

...
![Mockup for feature A](https://d0.awsstatic.com/partner-network/QuickStart/datasheets/bitnami-wordpress-on-aws-architecture.png)
...

---

# Clone a repository

Use these steps to clone from Bitbucket, our client for using the repository git command-line free. You must have to clone this repo into your HTDOCS Directory, after clone all wordpress files, themes, plugins will comes to your local environment and then you have to import SQL file with your PHPMyADMIN, then your project will setup on your server. If, you still don't understand the workplan then follow the below commands :-

1. You’ll see the clone button right after the top heading. Click that button.
2. Now click on Copy button and paste it into Git Command Line, and perform following commands.
 
    git clone https://tekkiwebsolutions@bitbucket.org/tekkiwebsolutions/tws-2020.git
    git add .
    git commit -m "MESSAGE"
    git push origin **BRANCH NAME**

---

## Create a file

Next, you’ll add a new file to this repository.

1. Add new files or Directories anytime.
2. Give the file a filename of **file-name**.
3. Check with Git Status Command.
4. Perform same commands as we did into "Clone a respoitory Section"

Before you move on, go ahead and explore the repository. You've already seen the **Source** page, but check out the **Commits**, **Branches**, and **Settings** pages.

---

## Edit a file

You’ll start by editing any file to learn how to edit a file in Bitbucket.

1. Modify any file and then check with Git status command.
2. Perform same commands as we did into "Clone a respoitory Section"

---

# Push repository

In the morning, developer must have to do first thing, before start work on this project, first pull files from MASTER Branch (IF needed then resolve conflicts). Then, Developer will add files with git command push file with their name and push files at his own branch name like this.

**Dev 1 dev/BRANCH NAME**

    Hotfix / hotfix (Urgent Issues Branch)
    Feature / feature (Only for Features Branch)
    BugFix / bugfix (Testing Branch)

**Dev 2 dev/BRANCH NAME**

    Hotfix / hotfix (Urgent Issues Branch)
    Feature / feature (Only for Features Branch)
    BugFix / bugfix (Testing Branch)

**Dev 3 dev/BRANCH NAME**

    Hotfix / hotfix (Urgent Issues Branch)
    Feature / feature (Only for Features Branch)
    BugFix / bugfix (Testing Branch)

*We recommend that you to work into master branch and then push files into your Name Branch, when you want to work for next work, then again checkout with Master, and make your job done and push files for Nwe Branch Name for testing for to perform the tasks below. You can [watch our video](https://youtu.be/0ocf7u76WSo) for a full demo of all the steps in this tutorial. Open the video in a new tab to avoid leaving Bitbucket.*

---

**Workflow/Guideline**

**1.**  In the morning every day please check your Bitrix Tasks for the scrum purposes.  

**2.** Always remember to check your pending tickets in Bitbucket or the corresponding project.

**3.** In order to create the branch please use:

First check you are in master branch.

> `git status`

Pull last changes, is important to create a branch using the latest master version

> `git pull`

Now you can create your new branch

> `git checkout -b <template>`

**4.** When you are ready or when the day is ending, please **ALWAYS** add, commit and push your changes.

Adding all changes to local in the current directory. Check your current directory using pwd.

> `git add .`

Commit the changes.

> `git commit -m <descriptive message of your work and status>`

Push the changes in order to sync the your local and the gitlab repo.

> `git push`

Note: When branch is new it needs create the remote reference.

**5.** In case of testing please use the dev server and docker instances.

**6.** Once you are ready please ask your ** merge or pull request ** , in case of help please let the team know.

Remember always check the changes and commit when a merge request is in progress.

**7.** Enjoy the trip!